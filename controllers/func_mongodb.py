from pymongo import MongoClient
import os
from dotenv import load_dotenv
load_dotenv("../")
import pandas as pd

from bson.objectid import ObjectId
from bson.json_util import dumps, loads

class MongoDB():
  def __init__(self):
    self.fastapi = MongoClient(os.environ.get("MONGODB_URL")).test.fastapi
  
def get_all_items():
  db = MongoDB().fastapi
  finds = db.find()
  return dumps(finds)

def get_item(item_id):
  db = MongoDB().fastapi
  find = db.find_one({"_id":ObjectId(item_id)})
  return dumps(find)

def insert_data(data):
  MongoDB().fastapi.insert_one(data)
  return ""

def update_item(item_id, data):
  print(data)
  MongoDB().fastapi.update_one({"_id":ObjectId(item_id)}, {'$set': data})
  return ""

def delete_item(item_id):
  MongoDB().fastapi.delete_one({"_id":ObjectId(item_id)})
  return ""