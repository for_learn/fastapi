from fastapi import FastAPI, Body
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
import os
from dotenv import load_dotenv
load_dotenv()
from pydantic import BaseModel
from controllers import func_mongodb
import json

app = FastAPI()
port_num = os.environ.get("APP_PORT") or 80
app.add_middleware(
  CORSMiddleware,
  allow_origins=[],
  allow_credentials=True,
  allow_methods=["*"],
  allow_headers=["*"],
)

class Data(BaseModel):
  banana: str
  orange: str

@app.get('/')
async def hello():
  """
  Hello this is root access 
  """
  return "Test FastAPI"

@app.get("/items")
async def get_all_items():
  datas = func_mongodb.get_all_items()
  return datas

@app.get("/items/{item_id}")
async def get_item(item_id:str):
  datas = func_mongodb.get_item(item_id)
  return datas

@app.post("/items")
async def create_items(data:Data):
  print(data)
  func_mongodb.insert_data(data)
  return "post"

@app.put("/items/{item_id}")
async def update_item(item_id:str, data:Data):
  print(item_id)
  func_mongodb.update_item(item_id, data)
  return "update"

@app.delete("/items/{item_id}")
async def delete_item(item_id:str):
  func_mongodb.delete_item(item_id)
  return "delete"

if __name__ == "__main__":
  uvicorn.run("api:app", host="0.0.0.0", port=int(port_num), reload=True)